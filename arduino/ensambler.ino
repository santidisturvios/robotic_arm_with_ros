#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

const int mpuAddress1 = 0x68;  // Puede ser 0x68 o 0x69
const int mpuAddress2 = 0x69;  // Puede ser 0x68 o 0x69
MPU6050 mpu1(mpuAddress1);
MPU6050 mpu2(mpuAddress2);
const int sensorPin1 = A0;
const int sensorPin2 = A1;
const int sensorPin3 = A2;
const int sensorPin4 = A3;
const int sensorPin5 = A4;
const int sensorPin6 = A5;
int ax, ay, az;
int gx, gy, gz;
double sensorValue1, sensorValue2, sensorValue3, sensorValue4, sensorValue5, sensorValue6 ;


void setup() {
  Serial.begin(115200);
  Wire.begin();
  mpu1.initialize();
  mpu2.initialize();
  Serial.println(mpu1.testConnection() ? F("IMU iniciado correctamente 1") : F("Error al iniciar IMU 1"));
  Serial.println(mpu2.testConnection() ? F("IMU iniciado correctamente 2") : F("Error al iniciar IMU 2"));

}

void loop() {
  double envio1 = datos_jostick();
  int envio2 = datos_acelerometro();
  float envio = envio2*1000000 + envio1;
  Serial.println(envio);
  delay(5);
}

int datos_acelerometro(){
  
   // Read acelerations 
   mpu1.getAcceleration(&ax, &ay, &az); 
   //Calculate angles of inclination link 2
   float accel_ang_x_1 = 86 - atan(ax / sqrt(pow(ay, 2) + pow(az, 2)))*(180.0 / 3.1416);
   
   mpu2.getAcceleration(&ax, &ay, &az);
   //Calculate angles of inclination link 1
   float accel_ang_y_2 = 86 + atan(ay / sqrt(pow(ax, 2) + pow(az, 2)))*(180.0 / 3.1416);
   int inclinaciones = int(accel_ang_x_1) + int(accel_ang_y_2)*100;
   
    

   return inclinaciones;
  }


double datos_jostick(){

  sensorValue1 = analogRead(sensorPin1);
  sensorValue2 = analogRead(sensorPin2);
  sensorValue3 = analogRead(sensorPin3);
  sensorValue4 = analogRead(sensorPin4);
  sensorValue5 = analogRead(sensorPin5);
  sensorValue6 = analogRead(sensorPin6);

  if(sensorValue1<=400){sensorValue1=1;}
  else{
  if(sensorValue1>=600){sensorValue1=2;}
  else{sensorValue1=0;}}
  
  if(sensorValue2<=400){sensorValue2=1;}
  else{
  if(sensorValue2>=600){sensorValue2=2;}
  else{sensorValue2=0;}}
  if(sensorValue3<=400){sensorValue3=1;}
  else{
  if(sensorValue3>=600){sensorValue3=2;}
  else{sensorValue3=0;}}
  
  if(sensorValue4<=400){sensorValue4=1;}
  else{
  if(sensorValue4>=600){sensorValue4=2;}
  else{sensorValue4=0;}}
  if(sensorValue5<=400){sensorValue5=1;}
  else{
  if(sensorValue5>=600){sensorValue5=2;}
  else{sensorValue5=0;}}

  if(sensorValue6<=400){sensorValue6=1;}
  else{
  if(sensorValue6>=600){sensorValue6=2;}
  else{sensorValue6=0;}}
  
  double envio = sensorValue6*100000 + sensorValue5*10000 + sensorValue4*1000 + sensorValue3*100 + sensorValue1 *10 + sensorValue2;
  return envio;
  
  }
