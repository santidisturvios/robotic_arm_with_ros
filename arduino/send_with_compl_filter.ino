//GND - GND
//VCC - VCC
//SDA - Pin A4
//SCL - Pin A5
 
#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

const int sensorPin1 = A0;
const int sensorPin2 = A1;
const int sensorPin3 = A2;
const int sensorPin4 = A3;
const int sensorPin5 = A4;
const int sensorPin6 = A5;

const int mpuAddress2 = 0x68;  // Puede ser 0x68 o 0x69
const int mpuAddress1 = 0x69;  // Puede ser 0x68 o 0x69
MPU6050 mpu2(mpuAddress2);
MPU6050 mpu1(mpuAddress1);

double sensorValue1, sensorValue2, sensorValue3, sensorValue4, sensorValue5, sensorValue6 ; 
int ax2, ay2, az2;
int gx2, gy2, gz2;
int ax1, ay1, az1;
int gx1, gy1, gz1;
 
long tiempo_prev_2;
long tiempo_prev_1;
float dt_2;
float dt_1;
float ang_y_2, ang_y_prev_2;
float ang_x_1, ang_x_prev_1;
float ang_z_1, ang_z_prev_1; 
float ang_z_2, ang_z_prev_2;

 
void setup()
{
   Serial.begin(115200);
   Wire.begin();
   mpu1.initialize();
   mpu2.initialize();
   Serial.println(mpu1.testConnection() ? F("IMU iniciado correctamente") : F("Error al iniciar IMU"));
   Serial.println(mpu2.testConnection() ? F("IMU iniciado correctamente") : F("Error al iniciar IMU"));
}

 
void loop() 
{
   // Leer las aceleraciones y velocidades angulares
   mpu2.getAcceleration(&ax2, &ay2, &az2);
   mpu2.getRotation(&gx2, &gy2, &gz2);
   mpu1.getAcceleration(&ax1, &ay1, &az1);
   mpu1.getRotation(&gx1, &gy1, &gz1);
 
   updateFiltered_2();
   updateFiltered_1();
 
   float envio1 = datos_jostick()+10000000;


   if(ang_z_1>=0){Serial.print(int(200+85-ang_x_1));} //add the cuadrant
   else{Serial.print(int(100+86-ang_x_1));}


   if(ang_z_2>=1){Serial.print(200+int(87+ang_y_2));} //add the cuadrant
   else{Serial.print(100+int(87+ang_y_2));}

   Serial.println(envio1);
   delay(10);
}



void updateFiltered_2()
{
   dt_2 = (millis() - tiempo_prev_2) / 1000.0;
   tiempo_prev_2 = millis();
 
   //Calcular los ángulos con acelerometro
   float accel_ang_y_2 = atan(-ax2 / sqrt(pow(ay2, 2) + pow(az2, 2)))*(180.0 / 3.14);
   float accel_ang_z_2 = atan(-az2 / sqrt(pow(ay2, 2) + pow(ax2, 2)))*(180.0 / 3.14);
 
   //Calcular angulo de rotación con giroscopio y filtro complementario
   ang_y_2 = 0.98*(ang_y_prev_2 + (gy2 / 131)*dt_2) + 0.02*accel_ang_y_2;
   ang_z_2 = 0.98*(ang_z_prev_2 + (gz2 / 131)*dt_2) + 0.02*accel_ang_z_2;
   ang_y_prev_2 = ang_y_2;
   ang_z_prev_2 = ang_z_2;
}

void updateFiltered_1()
{
   dt_1 = (millis() - tiempo_prev_1) / 1000.0;
   tiempo_prev_1 = millis();
 
   //Calcular los ángulos con acelerometro
   float accel_ang_x_1 = atan(-ay1 / sqrt(pow(ax1, 2) + pow(az1, 2)))*(180.0 / 3.14);
   float accel_ang_z_1 = atan(-az1 / sqrt(pow(ax1, 2) + pow(ay1, 2)))*(180.0 / 3.14);
   //Calcular angulo de rotación con giroscopio y filtro complementario
   ang_x_1 = 0.98*(ang_x_prev_1 + (gx1 / 131)*dt_1) + 0.02*accel_ang_x_1;
   ang_z_1 = 0.98*(ang_z_prev_1 + (gz1 / 131)*dt_1) + 0.02*accel_ang_z_1;
   ang_x_prev_1 = ang_x_1;
   ang_z_prev_1 = ang_z_1;
}

double datos_jostick(){

  sensorValue1 = analogRead(sensorPin1);
  sensorValue2 = analogRead(sensorPin2);
  sensorValue3 = analogRead(sensorPin3);
  sensorValue4 = analogRead(sensorPin4);
  sensorValue5 = analogRead(sensorPin5);
  sensorValue6 = analogRead(sensorPin6);

  if(sensorValue1<=400){sensorValue1=1;}
  else{
  if(sensorValue1>=600){sensorValue1=2;}
  else{sensorValue1=0;}}
  
  if(sensorValue2<=400){sensorValue2=1;}
  else{
  if(sensorValue2>=600){sensorValue2=2;}
  else{sensorValue2=0;}}
  if(sensorValue3<=400){sensorValue3=1;}
  else{
  if(sensorValue3>=600){sensorValue3=2;}
  else{sensorValue3=0;}}
  
  if(sensorValue4<=400){sensorValue4=1;}
  else{
  if(sensorValue4>=600){sensorValue4=2;}
  else{sensorValue4=0;}}
  if(sensorValue5<=400){sensorValue5=1;}
  else{
  if(sensorValue5>=600){sensorValue5=2;}
  else{sensorValue5=0;}}

  if(sensorValue6<=400){sensorValue6=1;}
  else{
  if(sensorValue6>=600){sensorValue6=2;}
  else{sensorValue6=0;}}
  
  double envio = sensorValue6*100000 + sensorValue5*10000 + sensorValue4*1000 + sensorValue3*100 + sensorValue1 *10 + sensorValue2;
  return envio;
  
  }

