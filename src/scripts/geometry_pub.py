#!/usr/bin/env python


import rospy
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import MultiArrayDimension, Int32MultiArray
import math

def callback(msg):    
    #rospy.loginfo(msg.data)
    x, y, z, angle_link_1, angle_link_2 = calc_position(msg.data)


    
    pub2 = rospy.Publisher('noa', Float32MultiArray, queue_size=1)
    mat = Float32MultiArray()
    mat.layout.dim.append(MultiArrayDimension())
    mat.layout.dim[0].label = "noa"
    mat.layout.dim[0].size =3
    mat.data = [0]*9
    
    mat.data[0] =  math.cos(angle_link_1+angle_link_2)
    mat.data[1] =  - math.sin(angle_link_1+angle_link_2)
    mat.data[2] =  math.sin(0)
    mat.data[3] =  math.cos(angle_link_1+angle_link_2)
    mat.data[4] =  - math.sin(angle_link_1+angle_link_2)
    mat.data[5] =  - math.cos(0)
    mat.data[6] =  math.sin(angle_link_1+angle_link_2)
    mat.data[7] =  math.cos(angle_link_1+angle_link_2)
    mat.data[8] =  0
    
    pub2.publish(mat)
    
    pub = rospy.Publisher('geometria_ang', Pose, queue_size=1)     
    p = Pose()
    p.position.x = x
    p.position.y = 0.0
    p.position.z = z
    # Make sure the quaternion is valid and normalized
    p.orientation.x = 0.5*math.sqrt(abs(mat.data[0]-mat.data[4]-mat.data[8]+1))
    p.orientation.y = 0.5*math.sqrt(abs(-mat.data[0]+mat.data[4]-mat.data[8]+1))
    p.orientation.z = 0.5*math.sqrt(abs(-mat.data[0]-mat.data[4]+mat.data[8]+1))
    p.orientation.w = 0.5*math.sqrt(abs(mat.data[0]+mat.data[4]+mat.data[8]+1))
    pub.publish(p)

def calc_position(angulos_bruto):
    angle_1,angle_2 = math.modf(angulos_bruto / 1000)
  

    cuadrante_2 = int(round(angle_1*10))# sometimes instead 1 is 0.9999 inside python
    cuadrante_1 = int(angle_2/100)
    angle_link_1 = int(angle_2) - ((int(angle_2/100))*100)
    angle_link_2 = int(round(angle_1*1000))
    angle_link_2 = angle_link_2 - ((int(angle_link_2/100))*100)
 

    if cuadrante_1==1 and cuadrante_2==1:
        angle_link_2 = angle_link_2 - angle_link_1
    if cuadrante_1==1 and cuadrante_2==2:
        angle_link_2 = -(angle_link_1 + angle_link_2)
    if cuadrante_1==2 and cuadrante_2==1:
        angle_link_2= angle_link_1 + angle_link_2
    if cuadrante_1==2 and cuadrante_2==2:
        angle_link_2=angle_link_1 - angle_link_2
    
    
    
    if cuadrante_1==1:
        angle_link_1 = 90 - angle_link_1
    if cuadrante_1==2:
        angle_link_1 = 90 + angle_link_1

    rospy.loginfo("angulo 1: %f", angle_link_1)
    rospy.loginfo("cuadrante_1: %f", cuadrante_1)
    rospy.loginfo("angulo2: %f", angle_link_2)
    rospy.loginfo("cuadrante_2: %f", cuadrante_2)
    

    angle_link_1_rad = math.radians(angle_link_1)
    angle_link_2_rad = math.radians(angle_link_2)
    
    x=380*math.cos(angle_link_1_rad + angle_link_2_rad) + 220*math.cos(angle_link_1_rad)
    y=0
    z=380*math.sin(angle_link_1_rad + angle_link_2_rad) + 220*math.sin(angle_link_1_rad) + 230


    pub3 = rospy.Publisher('angles', Int32MultiArray, queue_size=1)
    angles = Int32MultiArray()
    angles.data = [0, int(angle_link_1), int(angle_link_2)]
    pub3.publish(angles)
    
    return x, y, z, angle_link_1_rad, angle_link_2_rad

def angles_subscriber():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('pose_publisher', anonymous=True)
    rospy.Subscriber("datos_angulos", Float64, callback) # The topic name must match the one on the publisher, also with the message type.
    rospy.spin() # spin() simply keeps python from exiting until this node is stopped with CTRL+C

if __name__ == '__main__':
    try:
        angles_subscriber()
    except rospy:
        pass