#!/usr/bin/env python
# ROS imports
import rospy
from std_msgs.msg import Float64
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

PWM_base = 22 #GPIO PWM BASE ROBOT
PWM_es1 = 18 #GPIO PWM ESLABON 1
PWM_es2 = 16 #GPIO PWM ESLABON 2
PWM_es3 = 12 #GPIO PWM ESLABON 2
PWM_wr = 11

DIR_base = 32 #GPIO DIRECCION BASE
DIR_es1_m1 = 36 #GPIO DIRECCION MOTOR 1 ESLABON 1
DIR_es1_m2 = 38 #GPIO DIRECCION MOTOR 2 ESLABON 1
DIR_es2 = 40 #GPIO DIRECCION MOTOR 2 ESLABON 2
DIR_es3 = 37 #GPIO DIRECCION MOTOR 2 ESLABON 2
DIR_wr = 35

DD = 100 # DUTY CICLE POR DEFECTO SIEMPRE 100

GPIO.setup(PWM_base, GPIO.OUT)
GPIO.setup(PWM_es1, GPIO.OUT)
GPIO.setup(PWM_es2, GPIO.OUT)
GPIO.setup(PWM_es3, GPIO.OUT)
GPIO.setup(PWM_wr, GPIO.OUT)

GPIO.setup(DIR_base, GPIO.OUT)
GPIO.setup(DIR_es1_m1, GPIO.OUT)
GPIO.setup(DIR_es1_m2, GPIO.OUT)
GPIO.setup(DIR_es2, GPIO.OUT)
GPIO.setup(DIR_es3, GPIO.OUT)
GPIO.setup(DIR_wr, GPIO.OUT)

GPIO.output(DIR_base, 0)
base = GPIO.PWM(PWM_base, 200) ##pin y frecuencia

GPIO.output(DIR_es1_m1, 0)
GPIO.output(DIR_es1_m2, 0)
eslabon = GPIO.PWM(PWM_es1,50)

GPIO.output(DIR_es2, 0)
eslabon2 = GPIO.PWM(PWM_es2, 100) ##pin y frecuencia

GPIO.output(DIR_es3, 0)
eslabon3 = GPIO.PWM(PWM_es3, 600) ##pin y frecuencia

GPIO.output(DIR_wr,0) ##turning sense
wrist = GPIO.PWM(PWM_wr, 100) ##frecuency of wrist


def callback(msg):
    rospy.loginfo("Dato recibido del jostick: %f", msg.data)
    jostick_1, jostick_2, jostick_3, jostick_4, jostick_5, jostick_6 = valor_jostick(msg.data)
    print "moviendo eslabon 3 del motor direccion", jostick_1    
    print "moviendo eslabon2 del motor direccion", jostick_2    
    print "moviendo eslabon del motor direccion", jostick_3    
    print "moviendo base del motor direccon", jostick_4
    print "moviendo muneca del motor direccion", jostick_5
    print "moviendo pinza", jostick_6
 
  
    if jostick_5 == 1: 
        wrist.start(50)
        GPIO.output(DIR_wr, 1)          
    if jostick_5 == 2:     
        wrist.start(50)
        GPIO.output(DIR_wr, 0)     
    if jostick_5 == 0:     
        wrist.start(0) 

    if jostick_4 == 1:      
        base.start(50)
        GPIO.output(DIR_base, 1)          
    if jostick_4 == 2:     
        base.start(50)
        GPIO.output(DIR_base, 0)     
    if jostick_4 == 0:     
        base.start(0)
        
        
    if jostick_3 == 1:    
       GPIO.output(DIR_es1_m1, 1)     
       GPIO.output(DIR_es1_m2, 0) 
       eslabon.start(50)
    if jostick_3 == 2:    
       GPIO.output(DIR_es1_m1, 0)     
       GPIO.output(DIR_es1_m2, 1) 
       eslabon.start(50)
    if jostick_3 == 0:    
       eslabon.start(0)
       
    if jostick_2 == 1: 
        eslabon2.start(50)
        GPIO.output(DIR_es2, 1)          
    if jostick_2 == 2:     
        eslabon2.start(50)
        GPIO.output(DIR_es2, 0)     
    if jostick_2 == 0:     
        eslabon2.start(0)    
       
    if jostick_1 == 1: 
        eslabon3.start(50)
        GPIO.output(DIR_es3, 1)          
    if jostick_1 == 2:     
        eslabon3.start(50)
        GPIO.output(DIR_es3, 0)     
    if jostick_1 == 0:     
        eslabon3.start(0)  
        

   


def valor_jostick(numero):
    cmil = int((numero // 100000))
    numero = numero - (cmil * 100000)  
    dmil = int((numero // 10000))
    numero = numero - (dmil * 10000)   
    umil = int((numero // 1000))
    numero = numero - (umil * 1000)
    centenas = int((numero // 100)) 
    numero = numero - (centenas * 100)
    decenas = int((numero // 10)) 
    numero = numero - (decenas * 10)
    unidades = numero
    
    jostick_1 = int(umil)
    jostick_2 = int(centenas)
    jostick_3 = int(decenas)
    jostick_4 = int(unidades)
    jostick_5 = int(dmil)
    jostick_6 = int(cmil)

    return jostick_1, jostick_2, jostick_3, jostick_4, jostick_5, jostick_6
    
    
    
    
    

def movement_robot():
    rospy.init_node('jostick_subscriber', anonymous=True) #SANTY: ANONYMOUS TRUE LE DA UN NOMBRE UNICO
    rospy.Subscriber("datos", Float64, callback) # The topic name must match the one on the publisher, also with the message type.
    rospy.spin() # spin() simply keeps python from exiting until this node is stopped with CTRL+C


if __name__ == '__main__':
    movement_robot()
