#!/usr/bin/env python
# ROS imports
import rospy
from std_msgs.msg import Float64
import RPi.GPIO as gpio
from time import sleep



DIR = 20
DIR1 = 16
STEP = 21
CW =1
CCW =0
STEP2 = 26
DIR2 = 12

gpio.setmode(gpio.BCM)
gpio.setup(DIR, gpio.OUT)
gpio.setup(DIR1, gpio.OUT)
gpio.setup(STEP, gpio.OUT)
gpio.setup(STEP2, gpio.OUT)
gpio.setup(DIR2, gpio.OUT)
gpio.output(DIR,CW)
gpio.output(DIR1,CCW)
gpio.output(DIR2,CCW)


# This function executes any time a Float64 message arrives.
# It executes on its own thread.
def callback(msg):
    
    rospy.loginfo("Received from jostick: %f", msg.data)
    r = rospy.Rate(180)    
    
    if msg.data == 1:
        gpio.output(DIR,0)
        gpio.output(DIR1,1)
        for x in range(7):
            gpio.output(STEP,gpio.HIGH)
            r.sleep()
            gpio.output(STEP,gpio.LOW)
            r.sleep()
      
    if msg.data == 2:
        gpio.output(DIR,1)
        gpio.output(DIR1,0)
        for x in range(7):
            gpio.output(STEP,gpio.HIGH)
            r.sleep()
            gpio.output(STEP,gpio.LOW)
            r.sleep()
    
def memory_subscriber():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('jostick_subscriber', anonymous=True)#SANTY: ANONYMOUS TRUE LE DA UN NOMBRE UNICO
    rospy.Subscriber("datos", Float64, callback) # The topic name must match the one on the publisher, also with the message type.
    rospy.spin() # spin() simply keeps python from exiting until this node is stopped with CTRL+C



if __name__ == '__main__':
    memory_subscriber()
