#!/usr/bin/env python
# ROS imports
import rospy
from std_msgs.msg import Float64, Int32MultiArray
import RPi.GPIO as GPIO
import PID

GPIO.setmode(GPIO.BOARD)

PWM_base = 22 #GPIO PWM BASE ROBOT
PWM_es1 = 18 #GPIO PWM ESLABON 1
PWM_es2 = 16 #GPIO PWM ESLABON 2
PWM_es3 = 12 #GPIO PWM ESLABON 2
PWM_wr = 11

DIR_base = 32 #GPIO DIRECCION BASE
DIR_es1_m1 = 36 #GPIO DIRECCION MOTOR 1 ESLABON 1
DIR_es1_m2 = 38 #GPIO DIRECCION MOTOR 2 ESLABON 1
DIR_es2 = 40 #GPIO DIRECCION MOTOR 2 ESLABON 2
DIR_es3 = 37 #GPIO DIRECCION MOTOR 2 ESLABON 2
DIR_wr = 35

DD = 100 # DUTY CICLE POR DEFECTO SIEMPRE 100

GPIO.setup(PWM_base, GPIO.OUT)
GPIO.setup(PWM_es1, GPIO.OUT)
GPIO.setup(PWM_es2, GPIO.OUT)
GPIO.setup(PWM_es3, GPIO.OUT)
GPIO.setup(PWM_wr, GPIO.OUT)

GPIO.setup(DIR_base, GPIO.OUT)
GPIO.setup(DIR_es1_m1, GPIO.OUT)
GPIO.setup(DIR_es1_m2, GPIO.OUT)
GPIO.setup(DIR_es2, GPIO.OUT)
GPIO.setup(DIR_es3, GPIO.OUT)
GPIO.setup(DIR_wr, GPIO.OUT)

GPIO.output(DIR_base, 0)
base = GPIO.PWM(PWM_base, 200) ##pin y frecuencia

GPIO.output(DIR_es1_m1, 0)
GPIO.output(DIR_es1_m2, 0)
eslabon = GPIO.PWM(PWM_es1,100)

GPIO.output(DIR_es2, 0)
eslabon2 = GPIO.PWM(PWM_es2, 50) ##pin y frecuencia


GPIO.output(DIR_es3, 0)
eslabon3 = GPIO.PWM(PWM_es3, 600) ##pin y frecuencia

GPIO.output(DIR_wr,0) ##turning sense
wrist = GPIO.PWM(PWM_wr, 100) ##frecuency of wrist

goal = 0
P = 5
I = 0.5
D = 2
pid = PID.PID(P, I, D)
pid.setSampleTime(1)


def callback(msg):

   
    
    rospy.loginfo("Dato recibido angulos %f", msg.data[2])

    #print("ingresa un angulo 1")
    #goal_angulo_1 = input("angulo 1 = ")
    pid.SetPoint = goal  
    
    
    pid.update(msg.data[2])
    frequency = pid.output
    print(pid.output)
    movement_link_2(frequency)




def movement_link_2(frequency):
    
    print("moviendo link 1")
    

        
    
    if frequency >= 0:
        frequency = max(min(frequency,400),50)
        direction = 0
    else:
        frequency = abs(min(max(frequency,-400),-50))
        direction = 1
    
    print("la frecuencia", frequency)
    print("la direccion", direction)
    
    
    eslabon2.start(50)
    eslabon2.ChangeFrequency(frequency)

    GPIO.output(DIR_es2, direction) #direction 0 right or 1 left
    
        
    
def stop_link_2():
    eslabon2.start(0)

def ask_goal():
    print("input angle goal:")
    global goal
    goal = input("angulo 1 = ")    
        

def movement_robot_form_data():    
    rospy.init_node('data_movement', anonymous=True) #SANTY: ANONYMOUS TRUE LE DA UN NOMBRE UNICO
    ask_goal()    
    rospy.Subscriber("angles", Int32MultiArray, callback) # The topic name must match the one on the publisher, also with the message type.
    rospy.spin() # spin() simply keeps python from exiting until this node is stopped with CTRL+C

if __name__ == '__main__':
    movement_robot_form_data()

